package com.company.yatsenko.util;

import com.company.yatsenko.dto.Developer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DeveloperMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Developer developer=new Developer();
        developer.setId(resultSet.getInt("developer_id"));
        developer.setName(resultSet.getString("developer_name"));
        developer.setSpeciality(resultSet.getString("developer_speciality"));
        developer.setExperience(resultSet.getDouble("developer_experience"));
        return developer;
    }
}
