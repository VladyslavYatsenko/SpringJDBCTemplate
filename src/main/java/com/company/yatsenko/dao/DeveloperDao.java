package com.company.yatsenko.dao;

import com.company.yatsenko.dto.Developer;

import javax.sql.DataSource;
import java.util.List;

public interface DeveloperDao {
    public void setDataSource(DataSource dataSource);
    public void createDeveloper(String name,String speciality,Integer experience);
    public Developer getDeveloperById(Integer id);
    public List<Developer> getDevelopers();
    public void removeDeveloper(Integer id);
    public void updateDeveloper(Integer id,String name,String speciality,Integer experience);
}
