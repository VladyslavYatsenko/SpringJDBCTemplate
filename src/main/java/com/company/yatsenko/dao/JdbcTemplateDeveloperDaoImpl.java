package com.company.yatsenko.dao;

import com.company.yatsenko.dto.Developer;
import com.company.yatsenko.util.DeveloperMapper;
import org.springframework.jdbc.core.JdbcTemplate;


import javax.sql.DataSource;
import java.util.List;

public class JdbcTemplateDeveloperDaoImpl implements DeveloperDao {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource=dataSource;
        this.jdbcTemplate=new JdbcTemplate(dataSource);
    }

    @Override
    public void createDeveloper(String name, String speciality, Integer experience) {
        String sql="INSERT INTO Developers (developer_name,developer_speciality,developer_experience) VALUES (?,?,?)";
        jdbcTemplate.update(sql,name,speciality,experience);
        System.out.println("Developer successfully created.\nName: " + name + ";\nSpecilaty: " +
                speciality + ";\nExperience: " + experience + "\n");
    }

    @Override
    public Developer getDeveloperById(Integer id) {
        String sql="SELECT *FROM Developers WHERE developer_id=?";
        Developer developer = (Developer) jdbcTemplate.queryForObject(sql, new Object[]{id}, new DeveloperMapper());
        return developer;
    }

    @Override
    public List<Developer> getDevelopers() {
        String sql = "SELECT * FROM Developers";
        List<Developer> developers = jdbcTemplate.query(sql, new DeveloperMapper());
        return developers;
    }

    @Override
    public void removeDeveloper(Integer id) {
        String sql = "DELETE FROM Developers WHERE developer_id = ?";
        jdbcTemplate.update(sql, id);
        System.out.println("Developer with id: " + id + " successfully removed");
    }

    @Override
    public void updateDeveloper(Integer id, String name, String speciality, Integer experience) {
        String sql = "UPDATE Developers SET developer_name = ?, developer_speciality = ?, developer_experience = ? WHERE developer_id = ?";
        jdbcTemplate.update(sql, name, speciality, experience, id);
        System.out.println("Developer with id: " + id + " successfully updated.");
    }
}
