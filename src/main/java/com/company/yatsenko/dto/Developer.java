package com.company.yatsenko.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Developer {
    private Integer id;
    private String name;
    private String speciality;
    private Double experience;
}
